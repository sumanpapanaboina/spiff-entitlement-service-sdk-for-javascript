/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default  {
    partnerSaleRegistrationId: 131,
    partnerRepUserId:'12345',
    url: 'https://dummy-url.com',
};